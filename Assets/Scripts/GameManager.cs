using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
  public bool isGameActive;
  public List<GameObject> targets;
  public TextMeshProUGUI scoreText;
  public TextMeshProUGUI gameOverText;
  public Button restartButton;
  public GameObject titleScreen;
  private int score;
  private float spawnRate = 1f;

  void Start()
  {
  }

  void Update()
  {

  }

  IEnumerator SpawnTarget()
  {
    while (isGameActive)
    {
      yield return new WaitForSeconds(spawnRate);
      int index = Random.Range(0, targets.Count);
      Instantiate(targets[index]);
    }
  }

  public void UpdateScore(int scoreToAdd)
  {
    score += scoreToAdd;
    scoreText.text = "Score: " + score;
  }

  public void GameOver()
  {
    isGameActive = false;
    gameOverText.gameObject.SetActive(true);
    restartButton.gameObject.SetActive(true);
  }

  public void RestartGame()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
  }

  public void StartGame(int difficulty)
  {
    isGameActive = true;
    score = 0;
    scoreText.text = "Score: " + score;
    titleScreen.gameObject.SetActive(false);
    spawnRate /= difficulty;

    StartCoroutine(SpawnTarget());
  }
}
